import React from "react";

const App = (): JSX.Element => {
  return (
    <div className="App">
      <h1>{{ cookiecutter.project_name }}</h1>
    </div>
  );
};

export default App;
