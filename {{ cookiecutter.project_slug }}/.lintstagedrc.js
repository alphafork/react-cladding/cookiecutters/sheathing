export default {
  'src/**/*.ts?(x)': [
    'eslint --cache --fix',
    () => 'tsc -p tsconfig.json --noEmit',
  ],
}
