#!/usr/bin/env bash
git init --initial-branch=main
npm install
npm run prepare
mkdir -p src/{components,contexts,hooks,pages,services,types,utils}
mkdir -p src/assets/{css,img}
