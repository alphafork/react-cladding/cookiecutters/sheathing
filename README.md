# Sheathing - React App Template

[Cookiecutter](https://github.com/cookiecutter/cookiecutter) template for
bootstrapping a React app.


## Quick Start

Install Cookiecutter if not alreay installed:

`pip install --user cookiecutter`

Generate React app using the Sheathing cookiecutter:

`cookiecutter https://gitlab.com/alphafork/react-cladding/cookiecutters/sheathing`


## Features
- A basic React app created using [Vite](https://github.com/vitejs/vite) with
  demo contents removed.
- AGPL-3.0-or-later as the default license if not specified otherwise.
- README.md template with the following sections:
  - Project name as main heading and short description.
  - License section (with link to LICENSE file if AGPL-3.0-or-later is used).
  - Contact section with author's name and email.
- Post-generate [cookiecutter
  hook](https://cookiecutter.readthedocs.io/en/stable/advanced/hooks.html) to
  autoamtically initialize git, set up git hooks and install all dependencies.
- Linting and formatting using [ESLint and
  prettier](https://gitlab.com/alphafork/react-cladding/utils/eslint-config-sheathing),
  configured to run automatically on staged files using
  [husky](https://github.com/typicode/husky) and
  [lint-staged](https://github.com/okonet/lint-staged).
- [PrimeReact](https://primereact.org) UI component libraries pre-installed.


## License

[GPL-3.0-or-later](LICENSE)


## Contact

[Alpha Fork Technologies](https://alphafork.com)

Email: [connect@alphafork.com](mailto:connect@alphafork.com)
